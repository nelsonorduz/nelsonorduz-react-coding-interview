import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message, Form, DatePicker } from 'antd';
import moment from 'moment';

import { withContextInitialized } from '../../../components/hoc';
import CompanyCard from '../../../components/molecules/CompanyCard';
import GenericList from '../../../components/organisms/GenericList';
import OverlaySpinner from '../../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../../components/hooks/usePersonInformation';

import { Company } from '../../../constants/types';
import { ResponsiveListCard } from '../../../constants';

const PersonEdit = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onFinish = async (values: any) => {
    const formattedValues = {
      ...data,
      ...values,
      birthday: values.birthday.format('YYYY-MM-DD'),
    };
    console.log('Success:', formattedValues);

    await save(formattedValues);
    router.back();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Form
        name="basic"
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        initialValues={{
          name: data.name,
          gender: data.gender,
          phone: data.phone,
          birthday: moment(data.birthday),
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <PageHeader
          onBack={router.back}
          title="Person"
          subTitle="Edit Profile"
          extra={[
            <Button type="primary" htmlType="submit">
              Save Changes
            </Button>,
          ]}
        >
          <Form.Item label="Name" name="name">
            <Input />
          </Form.Item>
          <Form.Item label="Gender" name="gender">
            <Input />
          </Form.Item>
          <Form.Item label="Phone" name="phone">
            <Input />
          </Form.Item>
          <Form.Item label="Birthday" name="birthday">
            <DatePicker />
          </Form.Item>
          <Form.Item></Form.Item>
        </PageHeader>
      </Form>
    </>
  );
};

export default withContextInitialized(PersonEdit);
