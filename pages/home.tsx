import React, { useState } from 'react';
import { PageHeader } from 'antd';
import { TeamOutlined } from '@ant-design/icons';

import { withContextInitialized } from '../components/hoc';
import { usePeople } from '../components/hooks/usePeople';

import Header, { DEFAULT_PAGE_SIZE } from '../components/molecules/ListHeader';
import GenericList from '../components/organisms/GenericList';
import PersonDisplay from '../components/molecules/PersonDisplay';

import { DISPLAY_MODE_CARD, DISPLAY_MODE_LIST_ITEM, Person } from '../constants/types';
import { useRouter } from 'next/router';
import { ResponsiveListCard } from '../constants';
import { useEffect } from 'react';

const Home = () => {
  const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
  const [displayMode, setDisplayMode] = useState(DISPLAY_MODE_CARD);
  const [cacheRead, setCacheRead] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const cachedSettings = window.localStorage.getItem('pageSettings');
      if (cachedSettings) {
        const parsedSettings = JSON.parse(cachedSettings);
        parsedSettings.page && setPageSize(parsedSettings.page);
        parsedSettings.display && setDisplayMode(parsedSettings.display);
      }
      setCacheRead(true);
    }
  }, []);

  const router = useRouter();
  const { data: people, totalItems, loading, execute } = usePeople();

  const isCard = displayMode === DISPLAY_MODE_CARD;
  const Component = isCard ? PersonDisplay.Card : PersonDisplay.ListItem;

  const onSettingChange = (type) => (value) => {
    let temp;

    switch (type) {
      case 'display':
        temp = { page: pageSize, display: value };
        window.localStorage.setItem('pageSettings', JSON.stringify(temp));
        setDisplayMode(value);
        break;
      case 'page':
        temp = { display: displayMode, page: value };
        window.localStorage.setItem('pageSettings', JSON.stringify(temp));
        setPageSize(value);
        break;
      default:
        break;
    }
  };

  if (!cacheRead) return <></>;

  return (
    <PageHeader avatar={{ icon: <TeamOutlined /> }} title="People list">
      <GenericList<Person>
        extra={isCard && ResponsiveListCard}
        data={people}
        loading={loading}
        handleLoadMore={() => execute(pageSize)}
        hasMore={people.length < totalItems}
        ItemRenderer={({ item }: any) => (
          <Component onEdit={(email) => router.push(`/person/${email}`)} item={item} />
        )}
        Header={
          <Header
            displayMode={displayMode}
            onDiplayModeChange={(newDisplayMode) => {
              onSettingChange('display')(newDisplayMode);
            }}
            pageSize={pageSize}
            title={`Displaying ${people.length} of ${totalItems}`}
            onPageSizeChange={onSettingChange('page')}
          />
        }
      />
    </PageHeader>
  );
};

export default withContextInitialized(Home);
